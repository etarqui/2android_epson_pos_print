package com.example.AndroidPrintTest;

import android.app.Activity;
import android.os.Bundle;
import com.epson.epsonio.DevType;
import com.epson.epsonio.EpsonIoException;
import com.epson.epsonio.Finder;
import com.epson.epsonio.IoStatus;

public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        printDoc();
    }

    private void printDoc() {
        int errStatus = IoStatus.SUCCESS;
        String[] mList = null;

        try {
            Finder.start(getBaseContext(), DevType.TCP, "192.168.192.168");
            mList = Finder.getResult();
            Finder.stop();

        } catch (EpsonIoException e) {
            errStatus = e.getStatus();
        }
    }
}
